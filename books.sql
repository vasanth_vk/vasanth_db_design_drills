-- Branch#
-- Branch_Addr
-- ISBN
-- Title
-- Author
-- Publisher
-- Num_copies

CREATE TABLE books
(
    ISBN VARCHAR(13) PRIMARY KEY,
    Title VARCHAR(255),
    Num_copies INT,
    AuthorID INT,
    BranchID INT,
    PublisherID INT,
    FOREIGN KEY(AuthorID) REFERENCES authors(AuthorID),
    FOREIGN KEY(BranchID) REFERENCES branches(BranchID),
    FOREIGN KEY(PublisherID) REFERENCES publishers(PublisherID)
);

CREATE TABLE authors
(
    AuthorID INT PRIMARY KEY,
    Author VARCHAR(100)
);

CREATE TABLE branches
(
    BranchID INT PRIMARY KEY,
    Branch_Addr VARCHAR(255)
);

CREATE TABLE publishers
(
    PublisherID INT PRIMARY KEY,
    Publisher VARCHAR(100)
)

