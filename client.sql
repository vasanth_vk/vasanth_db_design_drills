-- Client#
-- Name
-- Location
-- Manager#
-- Manager_name
-- Manager_location
-- Contract#
-- Estimated_cost
-- Completion_date
-- Staff#
-- Staff_name
-- Staff_location

CREATE TABLE clients
(
    ClientID INT PRIMARY KEY,
    ClientName VARCHAR(100),
    LocationID INT,
    FOREIGN KEY(LocationID) REFERENCES locations(LocationID)
);

CREATE TABLE locations
(
    LocationID INT PRIMARY KEY,
    LocationName VARCHAR(255)
);

CREATE TABLE staff
(
    StaffID INT PRIMARY KEY,
    StaffName VARCHAR(100),
    LocationID INT,
    FOREIGN KEY(LocationID) REFERENCES locations(LocationID)
);

CREATE TABLE managers
(
    ManagerID INT PRIMARY KEY,
    ManagerName VARCHAR(100),
    LocationID INT,
    StaffID INT,
    FOREIGN KEY(LocationID) REFERENCES locations(LocationID),
    FOREIGN KEY(StaffID)  REFERENCES staff(StaffID)
);

CREATE TABLE contracts
(
    ContractID INT PRIMARY KEY,
    ClientID INT,
    EstimatedCost FLOAT,
    CompletionDate DATE,
    ManagerID INT,
    FOREIGN KEY(ClientID) REFERENCES clients(ClientID),
    FOREIGN KEY (ManagerID) REFERENCES managers(ManagerID)
);


