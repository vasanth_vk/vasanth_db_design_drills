-- Doctor#
-- DoctorName
-- Secretary
-- Patient#
-- PatientName
-- PatientDOB
-- PatientAddress
-- Prescription#
-- Drug
-- Date
-- Dosage

CREATE TABLE doctor
(
    DoctorID INT PRIMARY KEY,
    DoctorName VARCHAR(100),
    FOREIGN KEY(SecretaryID) REFERENCES secretary(SecretaryID)
);


CREATE TABLE patient
(
    PatientID INT PRIMARY KEY,
    PatientName VARCHAR(100),
    PatientDOB DATE,
    PatientAdress VARCHAR(255),
    PrescriptionID INT,
    FOREIGN KEY(PrescriptionID) REFERENCES prescription(PrescriptionID)
);

CREATE TABLE prescription
(
    PrescriptionID INT PRIMARY KEY,
    PrescriptionDate DATE,
    FOREIGN KEY(DrugID) REFERENCES drug(DrugID),
    Dosage VARCHAR(100)
);

CREATE TABLE patientsofdoctor
(
    DoctorID INT,
    PatientID INT,
    FOREIGN KEY(DoctorID) REFERENCES doctor(DoctorID),
    FOREIGN KEY(PatientID) REFERENCES patient(PatientID)
);

CREATE TABLE secretary
(
    SecretaryID INT PRIMARY KEY,
    SecretaryName VARCHAR(100)
)


CREATE TABLE drug
(
    DrugID INT PRIMARY KEY,
    DrugName VARCHAR(100)
);