-- Patient#
-- Name
-- DOB
-- Address
-- Prescription#
-- Drug
-- Date
-- Dosage
-- Doctor
-- Secretary

CREATE TABLE patient
(
    PatientID INT PRIMARY KEY,
    PatientName VARCHAR(100),
    PatientDOB DATE,
    PrescriptionID INT,
    DoctorID INT,
    FOREIGN KEY(PrescriptionID) REFERENCES prescription(PrescriptionID),
    FOREIGN KEY(DoctorID) REFERENCES doctor(DoctorID)
);

CREATE TABLE prescription
(
    PrescriptionID INT PRIMARY KEY,
    PrescriptionDate DATE,
    DrugID INT,
    FOREIGN KEY(DrugID) REFERENCES drug(DrugID),
    Dosage VARCHAR(100)
);

CREATE TABLE doctor
(
    DoctorID INT PRIMARY KEY,
    DoctorName VARCHAR(100),
    SecretaryID INT,
    FOREIGN KEY (SecretaryID) REFERENCES secretary(SecretaryID)
);

CREATE TABLE secretary
(
    SecretaryID INT PRIMARY KEY,
    SecretaryName VARCHAR(100)
)


CREATE TABLE drug
(
    DrugID INT PRIMARY KEY,
    DrugName VARCHAR(100)
);